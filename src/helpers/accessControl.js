import React, {	Component } from 'react';
import {connect} from 'react-redux';
import Typography from '@material-ui/core/Typography';

export const accessControl = permissionsRequired => WrappepComponent => {
	const SecuredControl = class extends Component {

		render(){
			const {permissions} = this.props.user;
			const isAllow = permissionsRequired.every(p => permissions.indexOf(p) >= 0);
			if(!isAllow){
				return <Typography variant="caption"> No tiene permisos de acceso. </Typography>
			}
			return <WrappepComponent {...this.props} />
		}
	}

	return connect(state => ({user: state.user}))(SecuredControl); 
}