import React, {	Component } from 'react';
import {withRouter} from 'react-router-dom';
import Button from '@material-ui/core/Button';
import AppFrame from './../components/AppFrame';
import CustomersActions from './../components/CustomersActions';

class HomeContainer extends Component {

	handleOnClick = () => {
		this.props.history.push('/customers');
	}
	render(){
		return(
			<div>
				<AppFrame 
					header="Home"
					body={
						<div className="buttonCont">
							<CustomersActions>
								<Button className="button" onClick={this.handleOnClick}>Listado de Clientes</Button>
							</CustomersActions>
						</div>
					}
				/>
			</div>
		);
	}

};



export default withRouter(HomeContainer);