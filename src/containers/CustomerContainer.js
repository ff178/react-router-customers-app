import React, {	Component } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {SubmissionError} from 'redux-form';
import {Route, withRouter} from 'react-router-dom';
import AppFrame from './../components/AppFrame';
import CustomerEdit from './../components/CustomerEdit';
import CustomerData from './../components/CustomerData';
import {getCustomerByDni} from './../selectors/customers';
import {fetchCustomers} from './../actions/fetchCustomers';
import {updateCustomer} from './../actions/updateCustomer';
import {deleteCustomer} from './../actions/deleteCustomer';

class CustomerContainer extends Component {

	componentDidMount() {
		if (!this.props.customer){
			this.props.fetchCustomers();
		}
	}
	handleSubmit = values => {
		console.log(JSON.stringify(values));
		const {id} = values;
		return this.props.updateCustomer(id, values).then(r => {
			if (r.error) {
				throw new SubmissionError(r.payload);
			}
		});

	}

	handleOnBack = () => {
		this.props.history.goBack();
	}

	handleOnSubmitSuccess = () => {
		this.props.history.goBack();
	}

	handleDelete = id => {
		console.log("handleDelete");
		this.props.deleteCustomer(id).then(v => {this.props.history.goBack()});
	}

	renderCustomerControl = (isEdit, isDelete) => (
		isEdit ? <CustomerEdit {...this.props.customer} onSubmit={this.handleSubmit} onSubmitSuccess={this.handleOnSubmitSuccess} onBack={this.handleOnBack}/> : 
				 <CustomerData {...this.props.customer} onBack={this.handleOnBack} isDeleteAllow={!!isDelete} onDelete={this.handleDelete}/>
	)

	renderBody = () => (
		<Route path="/customers/:dni/edit" 
				children={
					({match: isEdit }) => (
						<Route path="/customers/:dni/delete" 
								children={
									({match: isDelete}) => (this.renderCustomerControl(isEdit, isDelete))
								} 
						/>
					)
				} 
		/>
	)

	render(){
		return(
			<div>
				<AppFrame 
					header={`Cliente ${this.props.dni}`}
					body={this.renderBody()}
				/>
			</div>
		);
	}

};

CustomerContainer.propTypes = {
	dni:  PropTypes.string.isRequired,
	customer: PropTypes.object,
	fetchCustomers: PropTypes.func.isRequired,
	updateCustomer: PropTypes.func.isRequired,
	deleteCustomer: PropTypes.func.isRequired,
};
	
const mapStateToProps = (state, props) => ({
	customer: getCustomerByDni(state, props)
});

export default withRouter(connect(mapStateToProps,{
	fetchCustomers,
	updateCustomer,
	deleteCustomer
})(CustomerContainer));