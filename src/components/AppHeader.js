import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';

const AppHeader = ({title}) => {
	return (
		<div className="AppHeader">
			<Typography variant='title'>{title}</Typography>
		</div>
	);

};

AppHeader.propTypes = {
	title: PropTypes.string.isRequired,
};

export default AppHeader;