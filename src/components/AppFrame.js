import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
import AppHeader from './AppHeader';

const AppFrame = ({header, body}) => {
	return (
		<div className="AppFrame">
			<AppBar position='sticky' color='inherit'>
				<Toolbar>
					<AppHeader title={header} />
				</Toolbar>
			</AppBar>
			<Grid item xs={12}>
				<AppBar position='sticky' color='inherit'>
         				<div className="body">{body}</div>
         		</AppBar>
       	 	</Grid>
		</div>
	);

};

AppFrame.propTypes = {
	header: PropTypes.string.isRequired,
	body: PropTypes.element.isRequired,

};

export default AppFrame;