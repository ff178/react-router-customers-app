import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import LinkStyle from '@material-ui/core/Link';

const CustomerListItem = ({name, editAction, deleteAction, urlPath, dni}) => {
	return (
		<div className="CustomerListItem">
			<Paper>
				<Table>
					<TableBody>
						<TableRow key={dni}>
							<TableCell className="field">
								<LinkStyle component="button" variant="body1">
									<Link to={`${urlPath}${dni}`}>{name}</Link>
								</LinkStyle>
							</TableCell>
							<TableCell className="field" align="right">
								<LinkStyle component="button" variant="body1">
									<Link to={`${urlPath}${dni}/edit`}>{editAction}</Link>
								</LinkStyle>
							</TableCell>
							<TableCell className="field" align="right">
								<LinkStyle component="button" variant="body1">
									<Link to={`${urlPath}${dni}/delete`}>{deleteAction}</Link>
								</LinkStyle>
							</TableCell>
						</TableRow>
					</TableBody>
				</Table>
			</Paper>
		</div>
	);

};

CustomerListItem.propTypes = {
	dni: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
	editAction: PropTypes.string.isRequired,
	deleteAction: PropTypes.string.isRequired,
	urlPath: PropTypes.string.isRequired,
};

export default CustomerListItem;