import React from 'react';
import PropTypes from 'prop-types';
import {reduxForm, Field} from 'redux-form';
import {Prompt} from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import {setPropsAsInitial} from './../helpers/setPropsAsInitial';
import {accessControl} from './../helpers/accessControl';
import {CUSTOMER_EDIT} from './../constants/permissions';
import CustomersActions from './../components/CustomersActions';


//Validaciones a nivel de campo
/*const isRequired = value => (
	!value && "Este campo es requerido"
);*/

const isNumber = value => (
	isNaN(Number(value)) && "el campo es númerico"
);

//Validaciones a nivel global
const validate = values => {
	const error = {};

	if (!values.name){
		error.name = "El campo nombre es requerido";
	}else if (!values.dni){
		error.dni = "El campo dni es requerido";
	}

	return error;
};

const renderField = ({input, meta, type, label}) => (
	<div>
		<Typography variant='overline'>{label}</Typography>
		<TextField {...input} type={!type ? "text" : type} margin="normal"/>
		{
			meta.touched && meta.error && <Typography variant='overline'>{meta.error}</Typography>
		}
	</div>
);

const toNum = value => value && Number(value);
//const toUp = value => value && value.toUpperCase();
//const toLow = value => value && value.toLowerCase();

const CustomerEdit = ({name, dni, age, handleSubmit, submitting, onBack, pristine, submitSucceeded}) => {
	return (
		<div className="CustomerEdit">
			<AppBar position='sticky' color='inherit'>
				<Toolbar>
					<Typography variant='title'>Edición de Datos del cliente {name}</Typography>
				</Toolbar>
			</AppBar>
			<form onSubmit={handleSubmit} className="form">
				<Field name="name" component={renderField} type="text" label={"Nombre"}></Field>	
				
				<Field name="dni" component={renderField} type="text"  label={"DNI"} ></Field>	
				
				<Field name="age" component={renderField} type="number" validate={isNumber} label={"Edad"} parse={toNum}></Field>	
				<CustomersActions>
					<div className="buttonContSub">
						<Button className="buttonSub" type="submit" disabled={pristine || submitting}>Guardar</Button> 
					</div>
					<div className="buttonContSub">
						<Button className="buttonSub" type="button" disabled={submitting} onClick={onBack}>Cancelar</Button>
					</div> 
				</CustomersActions>
				<Prompt when={!pristine && !submitSucceeded} message="Ups! No haz guardado los datos. Quieres continuar?" />
			</form>
		</div>
	);

};

CustomerEdit.propTypes = {
	name: PropTypes.string,
	dni: PropTypes.string,
	age: PropTypes.number,
	onBack: PropTypes.func.isRequired,

};

const CustomerEditForm = reduxForm(
	{
		form: 'CustomerEdit',
		validate

	})(CustomerEdit);


export default accessControl([CUSTOMER_EDIT])(setPropsAsInitial(CustomerEditForm));

