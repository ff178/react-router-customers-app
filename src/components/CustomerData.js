import React from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import CustomersActions from './../components/CustomersActions';
import {accessControl} from './../helpers/accessControl';
import {CUSTOMER_VIEW} from './../constants/permissions';

const CustomerData = ({id, name, dni, age, onBack, isDeleteAllow, onDelete}) => {
	return (
		<div className="CustomerData">
			<AppBar position='sticky' color='inherit'>
				<Toolbar>
					<Typography variant='title'>Datos del cliente {name}</Typography>
				</Toolbar>
			</AppBar>
			<Card className="Card">
				<Typography variant='overline'>Nombre: {name}</Typography>
				<Typography variant='overline'>DNI: {dni}</Typography>
				<Typography variant='overline'>Edad: {age}</Typography>
			</Card>
			<CustomersActions>
					<div className="buttonContData">
						<Button className="buttonSub"  onClick={onBack}>Atrás</Button>
					</div> 
					{isDeleteAllow && 
						<div className="buttonContData">
							<Button className="buttonSub"  onClick={() => onDelete(id)}>Eliminar</Button>
						</div>  
					}
			</CustomersActions>
		</div>
	);

};

CustomerData.propTypes = {
	id: PropTypes.string,
	name: PropTypes.string,
	dni: PropTypes.string,
	age: PropTypes.number,
	onBack: PropTypes.func.isRequired,
	isDeleteAllow: PropTypes.bool,
	onDelete: PropTypes.func, 
};

export default accessControl([CUSTOMER_VIEW])(CustomerData);