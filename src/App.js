import React, { Component } from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import HomeContainer from './containers/HomeContainer';
import CustomersContainer from './containers/CustomersContainer';
import CustomerContainer from './containers/CustomerContainer';
import NewCustomerContainer from './containers/NewCustomerContainer';
import './App.css';

class App extends Component {
  renderHome = () => <HomeContainer />;
  renderList = () => <CustomersContainer />;
  renderCustomer = (props) => <CustomerContainer dni={props.match.params.dni} />;
  renderNew = () => <NewCustomerContainer />;
  render() {
    return (
      <Router>
        <div className="App">
          <Route exact path="/" component={this.renderHome}/>
          <Route exact path="/customers" component={this.renderList}/>
          <Switch>
            <Route  path="/customers/new" component={this.renderNew}/>
            <Route  path="/customers/:dni" render={this.renderCustomer}/>
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
